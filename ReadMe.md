# Modified N Queens with blocks

* The problem is to place some queens in a grid, such that no two queens can attack each other
* A queen can attack another queen if it is in the same row or same column or same diagonal as the other queen
* The modification is that, some locations in the grid contain blocks (obstacles). A queen cannot attack another queen with a block in between

### Input and Output
* The input is a text file called ```input.txt```. The first line is the size of the n * n matrix. The second line is the number of queens to be placed. The matrix contains either zeros or twos. A zero represents an empty location and a two represents a block.
* The output file is called ```output.txt```. The first line contains the string "OK" if the specified number of queens can be successfully placed on the grid and "FAIL" if it is not possible to place those many queens.
* Refer to the sample input and output files

### How to Run ?
* The first line of the input file should indicate the method to be used. The method can be ```DFS```, ```BFS``` or ```SA```.
* Complile using ```javac NQueensProblem.java```. Run using ```java NQueensProblem```
* The program produces an ```output.txt``` file in the same directory from which it is run
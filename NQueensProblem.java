import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

class NQueensProblem {
	
 	static long start_time = System.currentTimeMillis()/1000;
	static int input_matrix2 [][];
	static int tree_count[];
	
	public static void main (String args[]) throws IOException
	{
		long start_time = System.currentTimeMillis();
		
		LizardData input_obj = readInput("input.txt");
		input_matrix2 = new int[input_obj.n][input_obj.n];
		
		input_matrix2 = input_obj.input_matrix;
		if (input_obj.method.trim().equalsIgnoreCase("dfs"))
			callDFS(input_obj);
		else if (input_obj.method.trim().equalsIgnoreCase("bfs"))
		{
			callBFS(input_obj);
		}
		else if (input_obj.method.trim().equalsIgnoreCase("sa"))
			callSA(input_obj);
		
		System.out.println((System.currentTimeMillis()-start_time)/1000);
	}
	
	public static void callDFS(LizardData input_obj) throws IOException
	{
		int tree_locations[] = new int[input_obj.n];
		Set<Integer> allPossible = getAllPossibleLocations(input_obj.input_matrix, tree_locations);
		OutputData2 op = solveDfs(allPossible, new HashSet<Integer>(), input_obj.p,
				input_obj.n, input_obj.input_matrix, tree_locations);
		writeToFile(op, input_obj.input_matrix);
	}
	
	public static void callBFS(LizardData input_obj) throws IOException
	{
		
		tree_count = new int[input_obj.n];
     	
        BFSHelper1 dfh = outerFunction(input_obj);
        writeToFile(dfh, input_obj.input_matrix);
     	
	}
	
	public static void callSA(LizardData input_obj) throws IOException
	{
		callSimulatedAnnealing(input_obj);
	}
	
	public static LizardData readInput(String filename)
	{
		int input_matrix [][] = null;
		LizardData input_obj = null;
		int line_no = 0;
		int n = 0;
		int p = 0;
		String method = "";
		int row_no = 0;
		boolean matrix_initialization_flag = false;
		String current = "";
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while ((current = br.readLine()) != null) 
			{
				if (line_no == 0)
				{
					method = current;
				}
				else if (line_no == 1)
				{
					n = Integer.parseInt(current);
				}
				else if (line_no == 2)
				{
					p = Integer.parseInt(current);
				}
				else if (matrix_initialization_flag == false)
				{
					matrix_initialization_flag = true;
					input_matrix = new int[n][n];
					for (int i=0; i<n; i++)
					{
						input_matrix[row_no][i] = Character.getNumericValue(current.charAt(i));
					}
					row_no++;
				}
				else
				{
					for (int j=0; j<n; j++)
					{
						input_matrix[row_no][j] = Character.getNumericValue(current.charAt(j));
					}
					row_no++;
				}
				line_no++;		
			}
			br.close();
			input_obj = new LizardData(method, n, p, input_matrix);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return input_obj;
	}
	
	public static Set<Integer> 
	getInvalidLocations(int i, int j, int input_matrix[][], Set<Integer> placed, int n)
{
	int dir1 = 0;
	int dir2 = 0;
	
	Set<Integer> invalid = new TreeSet<>();
	
	//Mark elements to the left as invalid
	for (int k=j-1; k>=0; k--)
	{
		if(input_matrix[i][k] == 2)
			break;
		invalid.add( (i*input_matrix.length)+k);
	}
	
	//Mark elements to the right as invalid
	for (int k=j+1; k<n; k++)
	{
		if(input_matrix[i][k] == 2)
			break;
		invalid.add((i*input_matrix.length)+k);
	}
	
	//Mark elements above as invalid
	for (int l=i-1; l>=0; l--)
	{
		if(input_matrix[l][j] == 2)
			break;
		invalid.add((l*input_matrix.length)+j);
	}
	
	//Mark elements below as invalid
	for (int l=i+1; l<n; l++)
	{
		if(input_matrix[l][j] == 2)
			break;
		invalid.add((l*input_matrix.length)+j);
	}
	
	dir1 = i;
	dir2 = j;
	
	while (dir1-1>=0 && dir2+1<n)
	{
		if(input_matrix[dir1-1][dir2+1] == 2)
		{
			break;
		}
		invalid.add(((--dir1)*input_matrix.length)+(++dir2));
	}
	
	dir1 = i;
	dir2 = j;
	
	while (dir1+1<n && dir2+1<n)
	{
		if(input_matrix[dir1+1][dir2+1] == 2)
		{
			break;
		}
		invalid.add(((++dir1)*input_matrix.length)+(++dir2));
	}
	
	dir1 = i;
	dir2 = j;
	
	while (dir1-1>=0 && dir2-1>=0)
	{
		if(input_matrix[dir1-1][dir2-1] == 2)
		{
			break;
		}
		invalid.add(((--dir1)*input_matrix.length)+(--dir2));
	}
	
	dir1 = i;
	dir2 = j;
	
	while (dir1+1<n && dir2-1>=0)
	{
		if(input_matrix[dir1+1][dir2-1] == 2)
		{
			break;
		}
		invalid.add(((++dir1)*input_matrix.length)+(--dir2));
	}
	
	invalid.addAll(placed);
	return invalid;
}


	public static Set<Integer> getAllPossibleLocations(int input_matrix[][], int tree_locations[])
	{
		int till_now = 0;
		Set<Integer> all_possible = new TreeSet<>();
		for (int row=0; row<input_matrix.length; row++)
		{
			tree_locations[row] = till_now;
			for (int col=0; col<input_matrix[0].length; col++)
			{
				if (input_matrix[row][col] == 0)
					all_possible.add(row*input_matrix.length + col);
				else if (input_matrix[row][col] == 2)
				{
					tree_locations[row] = till_now + 1;
					till_now +=1;
				}
			}
		}
		
		return all_possible;
	}
	
	public static OutputData2 solveDfs(Set<Integer> allPossible,
			Set<Integer> placed, int no_of_lizards, int n, 
			int input_matrix[][], int tree_locations[])
	{
		if(placed.size() == no_of_lizards)
			return new OutputData2(true, placed);
		
		Iterator<Integer> i = allPossible.iterator();
		while(i.hasNext())
		{
			int elem = i.next();
			i.remove();
			
			if (elem/n>0 && (no_of_lizards - placed.size()) > ((n - (elem/n)) + (tree_locations[n-1] - tree_locations[(elem/n)-1])))
			{
				return new OutputData2(false, placed);
			}
			placed.add(elem);
			Set<Integer> all_possible_locations_copy = new TreeSet<>(allPossible);
			Set<Integer> invalid_locations = getInvalidLocations
					(elem/n, elem%n, input_matrix, placed, n);
			all_possible_locations_copy.removeAll(invalid_locations);
			OutputData2 opd = solveDfs(all_possible_locations_copy, placed, 
					no_of_lizards, n, input_matrix, tree_locations);
			
			if(opd.success)
				return new OutputData2(true, placed);
			else
			{
				placed.remove(elem);
			}
		}
		
		return new OutputData2(false, placed);	
	}
	
	
	public static void writeToFile(OutputData2 op, int input_matrix[][]) throws IOException
	{
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
		if (op.success)
		{
			bw.write("OK\n");
			for(int el : op.placement)
			{
				input_matrix[el/input_matrix.length]
						[el%input_matrix.length] = 1;
			}
			for (int row=0; row<input_matrix.length; row++)
			{
				for (int col=0; col<input_matrix[0].length; col++)
				{
					bw.write(Integer.toString(input_matrix[row][col]));
				}
				bw.write("\n");
			}
		}
		else
		{
			bw.write("FAIL");
		}
		bw.close();
	}
	
	public static BFSHelper1 outerFunction(LizardData input_data)
	{
		
		int till_now = 0;
		Set<Integer> tree_locations = new TreeSet<>();
		List<Integer> master_valid_list = new ArrayList<>();
		
		//do pre-processing here, store the tree locations
		for (int k=0; k<input_data.n; k++)
		{
			tree_count[k] = till_now;
			for (int l=0; l<input_data.n; l++)
			{
				if (input_data.input_matrix[k][l]==2)
				{
					tree_locations.add((k*input_data.n)+l);
					tree_count[k] = till_now + 1;
					till_now +=1;
				}
				else
				{
					master_valid_list.add((k*input_data.n)+l);
				}
					
			}
		}
		
		for (int i = 0; i<input_data.n; i++)
		{
			Queue<BFSHelper2> ar = new LinkedList<>();
			for (int j = 0; j<input_data.n; j++)
			{
				if (input_data.input_matrix[i][j]!=2)
				{
					List<Integer> a = new ArrayList<>();
					a.add(i*input_data.n+j);
					BFSHelper2 some = new BFSHelper2(a,master_valid_list);
					ar.add(some);
				}
			}
			
			if (!ar.isEmpty())
			{
				BFSHelper1 jlt = intermediate(ar, tree_locations, input_data.n, master_valid_list, input_data.p);
				if(jlt!=null)
				{
				if (jlt.status)
				{
					return jlt;
				}
				}
			}

		}
		return null;
	}
	
	public static BFSHelper1 intermediate(Queue<BFSHelper2> ar, Set<Integer> tree_locations, int n, List<Integer> master_valid_list, int p)
	{
		while(!ar.isEmpty())
		{
			BFSHelper2 element = ar.remove();
			
			Queue<BFSHelper2> ar_copy1 = new LinkedList<>();
			int e = element.elem.get(0);
			List<Integer> valid_loc = findValid(e, tree_locations, n, master_valid_list);
			BFSHelper2 some = new BFSHelper2(element.elem, valid_loc);
			for (int h =0; h<valid_loc.size()/2; h++)
			{
				List<Integer> element_copy = new ArrayList<>(element.elem);
				element_copy.add(valid_loc.get(h));
				List<Integer> valids = findValid(valid_loc.get(h), tree_locations, n, some.valid_list);
				BFSHelper2 some2 = new BFSHelper2(element_copy,valids);
				ar_copy1.add(some2);
				
			}
			
			BFSHelper1 dfsh = callBFS(ar_copy1, n, tree_locations, p);
			if (dfsh!=null)
			{
			if(dfsh.status)
			{
				return dfsh;
			}
			}
			
			Queue<BFSHelper2> ar_copy2 = new LinkedList<>();
			
			for (int j=valid_loc.size()/2; j<valid_loc.size(); j++)
			{
				List<Integer> element_copy = new ArrayList<>(element.elem);
				element_copy.add(valid_loc.get(j));
				List<Integer> valids = findValid(valid_loc.get(j), tree_locations, n, some.valid_list);
				BFSHelper2 some2 = new BFSHelper2(element_copy,valids);
				ar_copy2.add(some2);
			}
			BFSHelper1 ansobj = callBFS(ar_copy2, n, tree_locations, p);
			if(ansobj!=null)
			{
			if(ansobj.status)
				return ansobj;
			}
			else
			{
				return null;
			}
		}
		return new BFSHelper1(false, null);
	}
	
	public static BFSHelper1 callBFS(Queue<BFSHelper2> queue, int n, Set<Integer> tree_locations, int p)
	{
		while (!queue.isEmpty())
		{
			BFSHelper2 some = queue.remove();
			for(int it : some.valid_list)
			{
				List <Integer> q = new ArrayList<>(some.elem);
				if ((it/n > 0) && (p - some.no_placed) > ((n - it/n)+ (tree_count[n-1] - tree_count[(it/n) - 1])))
					continue;
				q.add(it);
				List<Integer> valids = findValid(it, tree_locations, n, some.valid_list);
				BFSHelper2 some2 = new BFSHelper2(q,valids);
				queue.add(some2);
				if(q.size() == p)
				{
					return new BFSHelper1(true, q);
				}
					
			}
		}
		
		return new BFSHelper1(false, null);
	}
	
	public static List<Integer> findValid(int elem, 
			Set<Integer> tree_locations, int n, List<Integer> master_valid_list)
	{
		List<Integer> invalid = new ArrayList<>();
		
		invalid.add(elem);
		
		int i = elem/n;
		int j = elem%n;

		int dir1 = 0;
		int dir2 = 0;


		//Mark elements to the left as invalid
		for (int k=j-1; k>=0; k--)
		{
			if(tree_locations.contains((i*n)+k))
				break;
			invalid.add((i*n)+k);
		}

		//Mark elements to the right as invalid
		for (int k=j+1; k<n; k++)
		{
			if(tree_locations.contains((i*n)+k))
				break;
			invalid.add((i*n)+k);
		}

		//Mark elements above as invalid
		for (int l=i-1; l>=0; l--)
		{
			if(tree_locations.contains((l*n)+j))
				break;
			invalid.add((l*n)+j);
		}

		//Mark elements below as invalid
		for (int l=i+1; l<n; l++)
		{
			if(tree_locations.contains((l*n)+j))
				break;
			invalid.add((l*n)+j);
		}

		dir1 = i;
		dir2 = j;

		while (dir1-1>=0 && dir2+1<n)
		{
			if(tree_locations.contains(((dir1-1)*n)+(dir2+1)))
			{
				break;
			}
			invalid.add(((--dir1)*n)+(++dir2));
		}

		dir1 = i;
		dir2 = j;

		while (dir1+1<n && dir2+1<n)
		{
			if(tree_locations.contains(((dir1+1)*n)+(dir2+1)))
			{
				break;
			}
			invalid.add(((++dir1)*n)+(++dir2));
		}

		dir1 = i;
		dir2 = j;

		while (dir1-1>=0 && dir2-1>=0)
		{
			if(tree_locations.contains(((dir1-1)*n)+(dir2-1)))
			{
				break;
			}
			invalid.add(((--dir1)*n)+(--dir2));
		}

		dir1 = i;
		dir2 = j;

		while (dir1+1<n && dir2-1>=0)
		{
			if(tree_locations.contains(((dir1+1)*n)+(dir2-1)))
			{
				break;
			}
			invalid.add(((++dir1)*n)+(--dir2));
		

		}
		
		List <Integer> mvl = new ArrayList<>(master_valid_list);
		mvl.removeAll(invalid);
		
		return mvl;
	}


public static void callSimulatedAnnealing(LizardData input_data) throws IOException
{
	//contains the count of the number of trees in each row
	int tree_counts[] = new int[input_data.n];
	
	//contains the location of trees
	Set<String> tree_locations = new HashSet<>();
	
	//contains mapping of row with the available valid locations
	Map<Integer, List<Integer>> mapping = generateMapping(input_data.input_matrix, tree_counts, tree_locations);
	
	//locations where lizards have been placed
	List<String> lizard_placement = new ArrayList<>();
	
	//Generates a start state for the board and returns false if the start state cannot be created
	boolean state = generateStartState(input_data.n, input_data.p, mapping, tree_counts, lizard_placement );
	
	if (state)
	{
		SAOutput ans = simulatedAnnealing(tree_locations, tree_counts, mapping, lizard_placement,input_data.n);
		
		if (ans.cost == 0)
		{
         	for (String place : ans.placement)
			{
				System.out.print(place+" ");
			}
			OutputData3 opd = new OutputData3(true, ans.placement);
			writeToFile(opd, input_data.input_matrix);
		}
		
		if (ans.cost != 0)
		{
			List<String> ll = new ArrayList<>();
			OutputData3 opd = new OutputData3(false, ll);
			writeToFile(opd, input_data.input_matrix);
		}
		
	}
	else
	{
		List<String> ll = new ArrayList<>();
		OutputData3 opd = new OutputData3(false, ll);
		writeToFile(opd, input_data.input_matrix);
	}
	
}

public static void writeToFile(OutputData3 op, int input_matrix[][]) throws IOException
{
	BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
	if (op.success)
	{
		bw.write("OK\n");
		for(String el : op.placement)
		{
			String elemarr [] = el.split(",");
			input_matrix[Integer.parseInt(elemarr[0])]
					[Integer.parseInt(elemarr[1])] = 1;
		}
		
		for (int row=0; row<input_matrix.length; row++)
		{
			for (int col=0; col<input_matrix[0].length; col++)
			{
				bw.write(Integer.toString(input_matrix[row][col]));
			}
			bw.write("\n");
		}
	}
	else
	{
		bw.write("FAIL");
	}
	bw.close();
}

public static Map<Integer,List<Integer>> generateMapping(int input_matrix[][]
		, int tree_counts[], Set<String> tree_locations)
{
	int n = input_matrix.length;
	Map<Integer, List<Integer>> mapping = new HashMap<>();
	for (int i=0; i<n; i++)
	{
		int tree_count = 0;
		List<Integer> col_list = new ArrayList<>();
		for (int j=0; j<n; j++)
		{
			if (input_matrix[i][j] == 0)
			{
				col_list.add(j);
			}
			else
			{
				tree_locations.add(i+","+j);
				tree_count++;
			}
		}
		mapping.put(i, col_list);
		tree_counts[i] = tree_count;
	}
	
	return mapping;
}

public static boolean 
	generateStartState(int n, int p, Map<Integer, 
			List<Integer>> mapping, int tree_counts[], List<String> lizard_placement)
{
	
	for (int i=0; i<n; i++)
	{
		//Get the column list for the row
		List<Integer> col_list = mapping.get(i);
		
		//Only if the list is not empty, get a random index from the col_list
		if(!col_list.isEmpty())
		{
			int random_index = (int)(Math.random() * col_list.size());
			int col_no = col_list.get(random_index);
			lizard_placement.add(i+","+col_no);
			col_list.remove(random_index);
			mapping.put(i, col_list);
			p--;
		}
	}
	
	while (p > 0)
	{
		//Find a row with tree count more than 1
		for (int i=0; i<tree_counts.length; i++)
		{
			if (tree_counts[i] >= 1)
			{
				//Place a tree in that row
				//Again find a random available column
				List<Integer> col_list = mapping.get(i);
				if (col_list.size() > 0)
				{
					int random_index = (int )(Math.random() * col_list.size());
					int col_no = col_list.get(random_index);
					lizard_placement.add(i+","+col_no);
					col_list.remove(random_index);
					mapping.put(i, col_list);
					tree_counts[i] -= 1;
					p--;
					break;
				}
			}
			if (i == tree_counts.length-1 && p > 0)
				return false;
		}
	}
	return true;
}

public static int calculateCost(List<String> lizard_placement, Set<String> tree_locations)
{
	int cost = 0;
	for (int i = 0; i < lizard_placement.size(); i++)
	{
		String first_elem[] = lizard_placement.get(i).split(",");
		for (int j=i+1; j< lizard_placement.size(); j++)
		{
			String second_elem[] = lizard_placement.get(j).split(",");
			
			int row1 = Integer.parseInt(first_elem[0]);
			int col1 = Integer.parseInt(first_elem[1]); 
			int row2 = Integer.parseInt(second_elem[0]);
			int col2 = Integer.parseInt(second_elem[1]);
			
			if (row1 == row2)
			{
				boolean in_between_tree = false;
				if (col2 < col1)
				{
					int temp = col2;
					col2 = col1;
					col1 = temp;
				}
				for (int a = col1+1; a<col2; a++)
				{
					if (tree_locations.contains((row1+","+a)))
					{
						in_between_tree = true;
						break;
					}
				}
				if (!in_between_tree)
					cost++;
			}
			else if (col1 == col2)
			{
				boolean in_between_tree = false;
				if (row2 < row1)
				{
					int temp = row2;
					row2 = row1;
					row1 = temp;
				}
				for (int a = row1+1; a<row2; a++)
				{
					if (tree_locations.contains((a+","+col1)))
					{
						in_between_tree = true;
						break;
					}
				}
				
				if (!in_between_tree)
					cost++;
			}
			
			else if (Math.abs(row1 - row2) == Math.abs(col1 - col2))
			{
				if (row2 > row1 && col2 > col1)
				{
					boolean in_between_tree = false;
					row1++;
					col1++;
					while(row1 < row2 && col1 < col2)
					{
						if (tree_locations.contains(row1+","+col1))
						{
							in_between_tree = true;
							break;
						}
						row1++;
						col1++;
					}
					
					if(!in_between_tree)
						cost++;
				}
				else if (row2 < row1 && col2 > col1)
				{
					boolean in_between_tree = false;
					row1--;
					col1++;
					while(row1 > row2 && col1 < col2)
					{
						if (tree_locations.contains(row1+","+col1))
						{
							in_between_tree = true;
							break;
						}
						row1--;
						col1++;
					}
					
					if(!in_between_tree)
						cost++;
				}
				else if (row2 < row1 && col2 < col1)
				{
					boolean in_between_tree = false;
					row1--;
					col1--;
					while(row1 > row2 && col1 > col2)
					{
						if (tree_locations.contains(row1+","+col1))
						{
							in_between_tree = true;
							break;
						}
						row1--;
						col1--;
					}
					
					if(!in_between_tree)
						cost++;
				}
				else if (row2 > row1 && col2 < col1)
				{
					boolean in_between_tree = false;
					row1++;
					col1--;
					while(row1 < row2 && col1 > col2)
					{
						if (tree_locations.contains(row1+","+col2))
						{
							in_between_tree = true;
							break;
						}
						row1++;
						col1--;
					}
					
					if(!in_between_tree)
						cost++;
				}
			}
		}
	}
	return cost;
}

public static List<String> generateNextState(Set<String> tree_locations, List<String> lizard_placement, int n)
{
	//copy placed into another list
	List<String> placement_copy = new ArrayList<>(lizard_placement);
	boolean success = false;
	
	while (!success)
	{
		if (placement_copy.size() == 0)
			return null;
		
		//select an already placed lizard randomly
		int random_index = (int )(Math.random() * placement_copy.size());
		String random_lizard = placement_copy.get(random_index);
		
		//generate all of its neighbours and check validity of each neighbour
		String random_lizard_arr[] = random_lizard.split(",");
		int row = Integer.parseInt(random_lizard_arr[0]);
		int col = Integer.parseInt(random_lizard_arr[1]);
		
		List<String> valid_list = new ArrayList<>();
		
		//row + 1 same col
		int xx = row + 1;
		while (xx < n)
		{
			if (!placement_copy.contains((xx+","+col)) && !tree_locations.contains(xx+","+col))
			{
				valid_list.add((row+1)+","+col);
				break;
			}
			xx++;
		}
		
		//row -1 same col
		int yy = row - 1;
		while (yy>=0)
		{
			if (!placement_copy.contains(yy+","+col) && !tree_locations.contains(yy+","+col))
			{
				valid_list.add((yy)+","+col);
				break;
			}
			yy--;
		}
		
		//col+1 same row
		int zz = col + 1;
		while (zz<n)
		{
			if (!placement_copy.contains((row+","+zz)) && !tree_locations.contains(row+","+zz))
			{
				valid_list.add((row)+","+zz);
				break;
			}
			zz++;
		}
		
		//col-1 same row
		int ww = col - 1;
		while (ww>=0)
		{
			if (!placement_copy.contains(((row)+","+ww)) && !tree_locations.contains(((row)+","+ww)))
			{
				valid_list.add(((row)+","+(ww)));
				break;
			}
			ww--;
		}
		
		//row+1 col+1
		int bb = row + 1;
		int cc = col + 1;
		while (bb<n && cc<n)
		{
			if (!placement_copy.contains((bb+","+cc)) && !tree_locations.contains((bb+","+cc)))
			{
				valid_list.add(((bb)+","+(cc)));
				break;
			}
			bb++;
			cc++;
		}
		
		//row-1 col+1
		int dd = row - 1;
		int ee = col + 1;
		while (dd>=0 && ee<n)
		{
			if (!placement_copy.contains((dd+","+ee)) && !tree_locations.contains((dd+","+ee)))
			{
				valid_list.add((dd+","+ee));
				break;
			}
			dd--;
			ee++;
		}
		
		//row-1 col-1
		int ff = row - 1;
		int gg = col - 1;
		while (ff>=0 && gg>=0)
		{
			if (!placement_copy.contains((ff+","+gg)) && !tree_locations.contains((ff+","+gg)))
			{	
				valid_list.add(((ff)+","+(gg)));
				break;
			}
			ff--;
			gg--;
		}
		
		//row+1 col-1
		int hh = row+1;
		int ii = col-1;
		while (hh<n && ii>=0)
		{
			if (!placement_copy.contains((hh+","+ii)) && !tree_locations.contains((hh+","+ii)))
			{
				valid_list.add(hh+","+ii);
				break;
			}
			hh++;
			ii--;
		}
		
	   /* if valid then add to valid list
		* select a random valid neighbor and change the place of the queen to
		* this new valid location */
		if (!valid_list.isEmpty())
		{
			int random_index_2 = (int )(Math.random() * valid_list.size());
			String new_location = valid_list.get(random_index_2);
			
			placement_copy.remove(random_lizard);
			placement_copy.add(new_location);
			success = true;
		}
		else
		{
			/* if valid is empty, then select another placed lizard randomly
			 *but to do this, remove the current lizard from placed copy so that its
			 *not selected again */
			placement_copy.remove(random_lizard);
		}
		
	}
	
	//if placed_copy becomes empty, it means there is no next valid state
	//terminate the algorithm
	
	return placement_copy;
}

public static boolean acceptState(double outside_probability)
{
	//generate a random number
	double random_prob = Math.random();
	
	if (random_prob <= outside_probability)
		return true;
	else
		return false;
}



public static SAOutput simulatedAnnealing(Set<String> tree_locations, int tree_counts[],
		Map<Integer, List<Integer>> mapping, List<String> lizard_placement, int n)
{
	int initial_cost =  calculateCost(lizard_placement, tree_locations);
	int iteration = 1;
	double temperature = 4.0/(Math.log(iteration + 30000.0));
	int cost = initial_cost;
	double start_time = System.currentTimeMillis()/1000;
	while((System.currentTimeMillis()/1000 - start_time) < 50)
	{		
		if (iteration == 1)
			cost = initial_cost;
		else
			cost = calculateCost(lizard_placement, tree_locations);
		
		if (cost == 0)
			return new SAOutput(lizard_placement,cost);

		List<String> copy_of_lizard_placement2 = new ArrayList<>(lizard_placement);
		List<String> copy_of_lizard_placement = generateNextState(tree_locations, copy_of_lizard_placement2, n);

		int delta_e = cost - calculateCost(copy_of_lizard_placement, tree_locations); 
		if ( delta_e > 0 )
			lizard_placement = copy_of_lizard_placement;
		else
		{
			double outside_probability = Math.exp((delta_e-0.0001)/temperature);
			
			if (acceptState(outside_probability))
				lizard_placement = copy_of_lizard_placement;
		}
		iteration++;
		temperature = 4.0/(Math.log(iteration + 30000.0));
	}
	
	return new SAOutput(lizard_placement,cost);
	
}

public static void writeToFile(BFSHelper1 op, int input_matrix[][]) throws IOException
{
	BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
	if(op!=null)
    {
 	if (op.status)
	{
		bw.write("OK\n");
		for(int el : op.placed)
		{
			input_matrix[el/input_matrix.length]
					[el%input_matrix.length] = 1;
		}
		for (int row=0; row<input_matrix.length; row++)
		{
			for (int col=0; col<input_matrix[0].length; col++)
			{
				bw.write(Integer.toString(input_matrix[row][col]));
			}
			bw.write("\n");
		}
	}
    }
	else
	{
		bw.write("FAIL");
	}
    
	bw.close();
}

}

class LizardData
{
	String method;
	int n;
	int p;
	int input_matrix[][];
	
	public LizardData(String method, int n, int p, int input_matrix[][])
	{
		this.method = method;
		this.n = n;
		this.p = p;
		this.input_matrix = input_matrix;
	}
}

class BFSHelper2
{
	List<Integer> elem;
	List<Integer> valid_list;
	int no_placed;
	
	public BFSHelper2(List<Integer> elem, List<Integer> valid_list)
	{
		this.elem = elem;
		this.valid_list = valid_list;
		this.no_placed = valid_list.size();
	}

}

class OutputData2
{
	boolean success;
	Set<Integer> placement;
	
	public OutputData2(boolean success, Set<Integer> placement)
	{
		this.success = success;
		this.placement = placement;
	}

}

class OutputData3
{
	boolean success;
	List<String> placement;
	
	public OutputData3(boolean success, List<String> placement)
	{
		this.success = success;
		this.placement = placement;
	}

}

class BFSHelper1
{
	boolean status;
	List<Integer> placed;
	
	public BFSHelper1(boolean status, List<Integer> placed)
	{
		this.status = status;
		this.placed = placed;
	}
}